public class Life {

  int Days;
  int LifeSize;
  ArrayList<Cell> Cells;
  int memoryState = 0;
  boolean pause = false;

  public Life () {
    Days = 0;
    this.LifeSize = 1; //changed while zooming
    newLife();
  }

  boolean foundLife() {
    boolean foundLife = false;
    for (Cell cell : Cells) {
      if (cell.isAlive()) {
        foundLife = true;
        return true;
      }
    }
    return foundLife;
  }

  void copy(Life cellsRef) {
    for (int i = 0; i < cellsRef.Cells.size(); i++) {
      if (cellsRef.Cells.get(i).isAlive()) Cells.get(i).setAlive(true);
      else Cells.get(i).setAlive(false);
      Cells.get(i).setAliveNeighbNum(cellsRef.Cells.get(i).getAliveNeighbNum());
      Cells.get(i).dayDead = cellsRef.Cells.get(i).dayDead;
    }
  }

  void checkFlipping(float accelerometerZ)
  {
    if (accelerometerZ<-9) {
      pause = !pause;
      delay(1000);
    }
  }

  void changeLifeSize(int i)
  {
    LifeSize = i;
  }

  void newLife() {
    Cells = new ArrayList<Cell> ();
    for (int i = 0; i < LifeSize*LifeSize*widthCell*heightCell; i++)
    {
      Cells.add(new Cell((float)width/(widthCell*LifeSize), i, LifeSize*widthCell, LifeSize*LifeSize*widthCell*heightCell ));
    }
  }

  void clickCheck(float x, float y)
  {
    float index = (float)width/(LifeSize*widthCell*2);
    for (int i = 0; i < Cells.size(); i++)
    { //checking borders
      float xcorIn = index*(2*(i%(LifeSize*widthCell)));
      float xcorAf = index*(2*(((i+0.99)%(LifeSize*widthCell))));
      float ycorIn = index*(2*(i/(LifeSize*widthCell)));
      float ycorAf = index*(2*((i+LifeSize*widthCell)/(LifeSize*widthCell)));
      if ((xcorIn < x && x < xcorAf && ycorIn < y && y <ycorAf))
      {
        if (memoryState == 0) {
          if (Cells.get(i).isAlive()) {
            Cells.get(i).setAlive(false);
            Cells.get(i).updateNeighboursMinus(this);
            memoryState = 1;
          } else {
            Cells.get(i).setAlive(true);
            Cells.get(i).updateNeighboursPlus(this);
            memoryState = 2;
          }
        } else if (memoryState == 1)
        {
          if (Cells.get(i).isAlive()) {
            Cells.get(i).setAlive(false);
            Cells.get(i).updateNeighboursMinus(this);
          }
        } else if (memoryState == 2)
        {
          if (!Cells.get(i).isAlive()) {
            Cells.get(i).setAlive(true);
            Cells.get(i).updateNeighboursPlus(this);
          }
        }
      }
    }
  }


  //updating the list with cells, referencing to the cellsCopy
  void update(Life cellsCopy) {

    for (int i =0; i < Cells.size(); i++) {


      if (cellsCopy.Cells.get(i).isAlive()) {
        //Do when cell is alive
        if (cellsCopy.Cells.get(i).getAliveNeighbNum()<2 &&cellsCopy.Cells.get(i).getAliveNeighbNum()>=0)
        {
          Cells.get(i).setAlive(false);
          Cells.get(i).updateNeighboursMinus(this);
        }

        //2 or 3 alive neighbours

        else if ((cellsCopy.Cells.get(i).getAliveNeighbNum()==2) || (cellsCopy.Cells.get(i).getAliveNeighbNum()==3)) {
          if (cellsCopy.Cells.get(i).dayDead==Days) {
            Cells.get(i).setAlive(false);
            Cells.get(i).dayDead = 0;
            Cells.get(i).updateNeighboursMinus(this);
          } else {
            Cells.get(i).setAlive(true);
            cellsCopy.Cells.get(i).dayDead++;
          }
        }

        //more than 3
        else if ( cellsCopy.Cells.get(i).getAliveNeighbNum()>3) {
          Cells.get(i).setAlive(false);
          Cells.get(i).updateNeighboursMinus(this);
        }
      }

      //dead cell with 3 alive neighbours;
      else if (!cellsCopy.Cells.get(i).isAlive() &&(cellsCopy.Cells.get(i).getAliveNeighbNum()==3)) {
        Cells.get(i).setAlive(true);
        Cells.get(i).updateNeighboursPlus(this);
      }
    }
  }

  void memoryStateChange()
  {
    this.memoryState = 0;
  }

  void cellColorChange()
  {
    for(int i = 0; i<Cells.size(); i++){
      Cells.get(i).colorChange();
    }
  }

  //drawing a cell
  void draw()
  {
    float index = (float)width/(LifeSize*widthCell*2);
    for (int i =0; i < Cells.size(); i++) {
      pushMatrix();
      translate(index*(2*(i%(LifeSize*widthCell))), index*(2*(i/(LifeSize*widthCell))));
      Cells.get(i).draw();
      popMatrix();
    }
  }

  int LifeSize() {
    return Cells.size();
  }
}
public class Cell {

  private boolean alive = false;
  float size;
  int index; 
  int widthMax, maxNum;
  private int aliveNeighbNum;
  color cellCol = color(0, 255, 0);
  int dayDead; //Day of death (day of the next generation to be born)

  public Cell (float size, int ind, int widthNum, int maxNum) {
    dayDead = 0;
    this.size = size;
    this.index = ind; 
    this.widthMax = widthNum;
    this.maxNum = maxNum;
    aliveNeighbNum = 0;
  }

  void setAliveNeighbNum(int num) {
    this.aliveNeighbNum = num;
  }

  int getAliveNeighbNum() {
    return aliveNeighbNum;
  }

  boolean isAlive() {
    return alive;
  }
  boolean hasUp() { //true if has upper alive neighbour
    if (index<(widthMax-1)) return false;
    return true;
  }

  boolean hasLow() {
    if (index>(maxNum-widthMax-1)) return false;
    return true;
  }

  boolean hasRight() {
    if ((index >= (maxNum-1)) || ((index+1)%widthMax==0) ) return false;
    return true;
  }

  boolean hasLeft() {
    if ((index == 0 ) || ((index)%widthMax==0) ) return false;
    return true;
  }
  boolean hasUpLeft() {
    if ((index == 0 ) || ((index)%widthMax==0) ) return false; //check left
    if (index<(widthMax-1)) return false; //check up;
    return true;
  }   

  boolean hasUpRight() {
    if (index<(widthMax-1)) return false;
    if ((index >= (maxNum-1)) || ((index+1)%widthMax==0) ) return false;//right;


    //check up;
    return true;
  }   

  boolean hasLowLeft() {
    if ((index == 0 ) || ((index)%widthMax==0) ) return false; //check left
    if (index>(maxNum-widthMax)) return false;//check low
    return true;
  } 
  boolean hasLowRight() {
    if ((index >= (maxNum-1)) || ((index+1)%widthMax==0) ) return false;//right;
    if (index>(maxNum-widthMax-1)) return false; //check low
    return true;
  } 

  Cell getUp(Life cells) {
    return cells.Cells.get(index-widthMax);
  }

  Cell getLow(Life cells) {
    return cells.Cells.get(index+widthMax);
  }

  Cell getRight(Life cells) {
    return cells.Cells.get(index+1);
  }

  Cell getLeft(Life cells) {
    return cells.Cells.get(index-1);
  }

  Cell getUpRight(Life cells) {
    return cells.Cells.get(index-widthMax+1);
  }

  Cell getUpLeft(Life cells) {
    return cells.Cells.get(index-widthMax-1);
  }

  Cell getLowLeft(Life cells) {
    return cells.Cells.get(index+widthMax-1);
  }

  Cell getLowRight(Life cells) {
    return cells.Cells.get(index+widthMax+1);
  }

  void updateNeighboursPlus(Life cells) {
    if (hasUp()) getUp(cells).aliveNeighbNum++;
    if (hasLow()) getLow(cells).aliveNeighbNum++;
    if (hasLeft()) getLeft(cells).aliveNeighbNum++;
    if (hasRight()) getRight(cells).aliveNeighbNum++;
    if (hasUpLeft()) getUpLeft(cells).aliveNeighbNum++;
    if (hasUpRight()) getUpRight(cells).aliveNeighbNum++;
    if (hasLowRight()) getLowRight(cells).aliveNeighbNum++;
    if (hasLowLeft()) getLowLeft(cells).aliveNeighbNum++;
  }

  void updateNeighboursMinus(Life cells) {
    if (hasUp()) getUp(cells).aliveNeighbNum--;
    if (hasLow()) getLow(cells).aliveNeighbNum--;
    if (hasLeft()) getLeft(cells).aliveNeighbNum--;
    if (hasRight()) getRight(cells).aliveNeighbNum--;
    if (hasUpLeft()) getUpLeft(cells).aliveNeighbNum--;
    if (hasUpRight()) getUpRight(cells).aliveNeighbNum--;
    if (hasLowRight()) getLowRight(cells).aliveNeighbNum--;
    if (hasLowLeft()) getLowLeft(cells).aliveNeighbNum--;
  }

  void setAlive(boolean l)
  {
    this.alive = l;
  }

  void colorChange()
  {
    cellCol = color((int) random(0, 255), 255, (int) random(0, 255));
  }

  void draw() 
  {
    rectMode(CENTER);
    if (EDIT) {
      stroke(255);
      strokeWeight(0.50);
    } else {
      noStroke();
    }
    if (alive)
    {
      fill(0, 255, 0);
      fill(cellCol);
    } else 
    {
      fill(0);
    }
    rect(size/2, size/2, size-1, size);
  }
}
import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import android.view.MotionEvent; 
import ketai.ui.*; 
import ketai.sensors.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class hw6 extends PApplet {


/*==============================================*/
//press "p" for starting the timer
//press "e" for editing mode (5 fingers)
//press "c" for cleaning (shaking)
// + and - for zooming in and zooming out

/*==============================================*/





KetaiGesture gesture;
KetaiSensor sensor;

int changecheck=0;
boolean pinch=false;
float accelerometerX, accelerometerY, accelerometerZ;
boolean gotTwo = false;

Life cells; //for drawing
Life cellsCopy; //copy which is updating
int widthCell = 9; // # of minimum widthCell size(13,26,39,...)
int heightCell = 15; // # of minimum heightCell size(23,46,69,...)
float pinchSize = 1999;
boolean EDIT = true;


int Days = 0; //TIMER
public void setup()
{
  
  gesture = new KetaiGesture(this);
  sensor = new KetaiSensor(this);
  sensor.start();
  //size(370, 675); // Use this for testing on PC
  cells = new Life();
  cellsCopy = new Life();
}

public void draw()
{
  background(0);
  if (accelerometerX>15 || accelerometerX<-15) clean();

  if (EDIT) { //EDITING MODE
    cells.Days = 0;
    cellsCopy.Days = 0;
    cells.draw();
  } else { //PLAYING MODE
    if (cells.foundLife() || cellsCopy.foundLife()) { //if any cells are alive
      cells.Days++;
      cellsCopy.Days++;
   }
    cells.draw(); //draw cells
    cellsCopy.copy(cells); //cellsCopy = copy of cells
    cellsCopy.update(cells); ///update cellsCopy by referencing to cells
    cells.copy(cellsCopy); //swap: cells <-- cellsCopy
    delay(100);
  }

  fill(255);
  stroke(153);
  textSize(24);
  textAlign(CENTER, BOTTOM);
  text("Days: " + str(cells.Days), 50, height);
}

public class Life {

  int Days;
  int LifeSize;
  ArrayList<Cell> Cells;

  public Life () {
    Days = 0;
    this.LifeSize = 1; //changed while zooming
    newLife();
  }
  
  public boolean foundLife() {
    boolean foundLife = false;
    for (Cell cell : Cells) {
      if (cell.isAlive()) {
        foundLife = true;
        return true;
      }
    }
    return foundLife;
  }
  
  public void copy(Life cellsRef) {
    for (int i = 0; i < cellsRef.Cells.size(); i++) {
      if (cellsRef.Cells.get(i).isAlive()) Cells.get(i).setAlive(true);
      else Cells.get(i).setAlive(false);
      Cells.get(i).setAliveNeighbNum(cellsRef.Cells.get(i).getAliveNeighbNum());
      Cells.get(i).dayDead = cellsRef.Cells.get(i).dayDead;
    }
  }

  public void changeLifeSize(int i)
  {
    LifeSize = i;
  }

  public void newLife() {
    Cells = new ArrayList<Cell> ();
    for (int i = 0; i < LifeSize*LifeSize*widthCell*heightCell; i++)
    {
      Cells.add(new Cell((float)width/(widthCell*LifeSize), i, LifeSize*widthCell, LifeSize*LifeSize*widthCell*heightCell ));
    }
  }

  public void clickCheck(float x, float y)
  {
    float index = (float)width/(LifeSize*widthCell*2);
    for (int i = 0; i < Cells.size(); i++)
    { //checking borders
      float xcorIn = index*(2*(i%(LifeSize*widthCell)));
      float xcorAf = index*(2*(((i+1)%(LifeSize*widthCell))));
      float ycorIn = index*(2*(i/(LifeSize*widthCell)));
      float ycorAf = index*(2*((i+LifeSize*widthCell)/(LifeSize*widthCell)));
      if ((xcorIn < x && x < xcorAf && ycorIn < y && y <ycorAf))
      {
        if (Cells.get(i).isAlive()) {
          Cells.get(i).setAlive(false);
          Cells.get(i).updateNeighboursMinus(this);
        } else {
          Cells.get(i).setAlive(true);
          Cells.get(i).updateNeighboursPlus(this);
        }
      }
    }
  }

  //updating the list with cells, referencing to the cellsCopy
  public void update(Life cellsCopy) {

    for (int i =0; i < Cells.size(); i++) {


      if (cellsCopy.Cells.get(i).isAlive()) {
        //Do when cell is alive
        if (cellsCopy.Cells.get(i).getAliveNeighbNum()<2 &&cellsCopy.Cells.get(i).getAliveNeighbNum()>=0)
        {
          Cells.get(i).setAlive(false);
          Cells.get(i).updateNeighboursMinus(this);
        }

        //2 or 3 alive neighbours

        else if ((cellsCopy.Cells.get(i).getAliveNeighbNum()==2) || (cellsCopy.Cells.get(i).getAliveNeighbNum()==3)) {
          if (cellsCopy.Cells.get(i).dayDead==Days) {
            Cells.get(i).setAlive(false);
            Cells.get(i).dayDead = 0;
            Cells.get(i).updateNeighboursMinus(this);
          } else {
            Cells.get(i).setAlive(true);
            cellsCopy.Cells.get(i).dayDead++;
          }
        }

        //more than 3
        else if ( cellsCopy.Cells.get(i).getAliveNeighbNum()>3) {
          Cells.get(i).setAlive(false);
          Cells.get(i).updateNeighboursMinus(this);
        }
      }

      //dead cell with 3 alive neighbours;
      else if (!cellsCopy.Cells.get(i).isAlive() &&(cellsCopy.Cells.get(i).getAliveNeighbNum()==3)) {
        Cells.get(i).setAlive(true);
        Cells.get(i).updateNeighboursPlus(this);
      }
    }
  }

  //drawing a cell
  public void draw()
  {
    float index = (float)width/(LifeSize*widthCell*2);
    for (int i =0; i < Cells.size(); i++) {
      pushMatrix();
      translate(index*(2*(i%(LifeSize*widthCell))), index*(2*(i/(LifeSize*widthCell))));
      Cells.get(i).draw();
      popMatrix();
    }
  }

  public int LifeSize() {
    return Cells.size();
  }
}

public class Cell {

  private boolean alive = false;
  float size;
  int index; 
  int widthMax, maxNum;
  private int aliveNeighbNum;
  int dayDead; //Day of death (day of the next generation to be born)

  public Cell (float size, int ind, int widthNum, int maxNum) {
    dayDead = 0;
    this.size = size;
    this.index = ind; 
    this.widthMax = widthNum;
    this.maxNum = maxNum;
    aliveNeighbNum = 0;
  }

  public void setAliveNeighbNum(int num) {
    this.aliveNeighbNum = num;
  }

  public int getAliveNeighbNum() {
    return aliveNeighbNum;
  }

  public boolean isAlive() {
    return alive;
  }
  public boolean hasUp() { //true if has upper alive neighbour
    if (index<(widthMax-1)) return false;
    return true;
  }

  public boolean hasLow() {
    if (index>(maxNum-widthMax-1)) return false;
    return true;
  }

  public boolean hasRight() {
    if ((index >= (maxNum-1)) || ((index+1)%widthMax==0) ) return false;
    return true;
  }

  public boolean hasLeft() {
    if ((index == 0 ) || ((index)%widthMax==0) ) return false;
    return true;
  }
  public boolean hasUpLeft() {
    if ((index == 0 ) || ((index)%widthMax==0) ) return false; //check left
    if (index<(widthMax-1)) return false; //check up;
    return true;
  }   

  public boolean hasUpRight() {
    if (index<(widthMax-1)) return false;
    if ((index >= (maxNum-1)) || ((index+1)%widthMax==0) ) return false;//right;


    //check up;
    return true;
  }   

  public boolean hasLowLeft() {
    if ((index == 0 ) || ((index)%widthMax==0) ) return false; //check left
    if (index>(maxNum-widthMax)) return false;//check low
    return true;
  } 
  public boolean hasLowRight() {
    if ((index >= (maxNum-1)) || ((index+1)%widthMax==0) ) return false;//right;
    if (index>(maxNum-widthMax-1)) return false; //check low
    return true;
  } 

  public Cell getUp(Life cells) {
    return cells.Cells.get(index-widthMax);
  }

  public Cell getLow(Life cells) {
    return cells.Cells.get(index+widthMax);
  }

  public Cell getRight(Life cells) {
    return cells.Cells.get(index+1);
  }

  public Cell getLeft(Life cells) {
    return cells.Cells.get(index-1);
  }

  public Cell getUpRight(Life cells) {
    return cells.Cells.get(index-widthMax+1);
  }

  public Cell getUpLeft(Life cells) {
    return cells.Cells.get(index-widthMax-1);
  }

  public Cell getLowLeft(Life cells) {
    return cells.Cells.get(index+widthMax-1);
  }

  public Cell getLowRight(Life cells) {
    return cells.Cells.get(index+widthMax+1);
  }

  public void updateNeighboursPlus(Life cells) {
    if (hasUp()) getUp(cells).aliveNeighbNum++;
    if (hasLow()) getLow(cells).aliveNeighbNum++;
    if (hasLeft()) getLeft(cells).aliveNeighbNum++;
    if (hasRight()) getRight(cells).aliveNeighbNum++;
    if (hasUpLeft()) getUpLeft(cells).aliveNeighbNum++;
    if (hasUpRight()) getUpRight(cells).aliveNeighbNum++;
    if (hasLowRight()) getLowRight(cells).aliveNeighbNum++;
    if (hasLowLeft()) getLowLeft(cells).aliveNeighbNum++;
  }

  public void updateNeighboursMinus(Life cells) {
    if (hasUp()) getUp(cells).aliveNeighbNum--;
    if (hasLow()) getLow(cells).aliveNeighbNum--;
    if (hasLeft()) getLeft(cells).aliveNeighbNum--;
    if (hasRight()) getRight(cells).aliveNeighbNum--;
    if (hasUpLeft()) getUpLeft(cells).aliveNeighbNum--;
    if (hasUpRight()) getUpRight(cells).aliveNeighbNum--;
    if (hasLowRight()) getLowRight(cells).aliveNeighbNum--;
    if (hasLowLeft()) getLowLeft(cells).aliveNeighbNum--;
  }

  public void setAlive(boolean l)
  {
    this.alive = l;
  }

  public void draw() 
  {
    rectMode(CENTER);
    if (EDIT) {
      stroke(255);
      strokeWeight(0.50f);
    } else {
      noStroke();
    }
    if (alive)
    {
      fill(0, 255, 0);
    } else 
    {
      fill(0);
    }
    rect(size/2, size/2, size-1, size);
  }
}

public void onTap(float x, float y)
{
  if (pinch || !EDIT) return;
  cells.clickCheck(x, y);
}

public void onPinch(float x, float y, float d)
{
  if (pinch&&EDIT){
println("PINCH");
  pinchSize=pinchSize+d;
  if (pinchSize<0) pinchSize=0;
  if (pinchSize>1999) pinchSize=1999;
  int switchsize=PApplet.parseInt(pinchSize/400);

  cells.changeLifeSize(5-switchsize);
  cells.newLife();
  cellsCopy.changeLifeSize(5-switchsize);
  cellsCopy.newLife();
  delay(500);
}
}

public void mouseDragged()
{
  if (pinch || !EDIT) return;
  cells.clickCheck(mouseX, mouseY);
}

public boolean surfaceTouchEvent(MotionEvent event)
{
  super.surfaceTouchEvent(event);
  int numPointers = event.getPointerCount();
  //if (numPointers>=2) delay(100);
  if (numPointers==5) {
    //gotFive = true;
    change();
  }
  if (numPointers==2) gotTwo=true;
  if (event.getActionMasked() == MotionEvent.ACTION_UP) {
    if(gotTwo){ pinch = true; gotTwo = false; }
  }
  
  else pinch=false;

  return gesture.surfaceTouchEvent(event);
}

public void change()
{
  int temp=millis();
  if ((temp-changecheck)>500) EDIT=!EDIT;
  changecheck=temp;
}

public void clean()
{
  if (!EDIT || pinch) return;
  println("CLEANED");
  cells.newLife();
}

public void onAccelerometerEvent(float x, float y, float z)
{
  accelerometerX = x;
  accelerometerY = y;
  accelerometerZ = z;
}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "hw6" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}


/*==============================================*/
//press "p" for starting the timer
//press "e" for editing mode (5 fingers)
//press "c" for cleaning (shaking)
// + and - for zooming in and zooming out

/*==============================================*/

import android.view.MotionEvent;
import ketai.ui.*;
import ketai.sensors.*;

KetaiGesture gesture;
KetaiSensor sensor;

int changecheck=0;
boolean pinch=false;
float accelerometerX, accelerometerY, accelerometerZ;
boolean gotTwo = false;

Life cells; //for drawing
Life cellsCopy; //copy which is updating
int widthCell = 9; // # of minimum widthCell size(13,26,39,...)
int heightCell = 15; // # of minimum heightCell size(23,46,69,...)
float pinchSize = 1999;
boolean EDIT = true;


int Days = 0; //TIMER
void setup()
{
  fullScreen();
  gesture = new KetaiGesture(this);
  sensor = new KetaiSensor(this);
  sensor.start();
  //size(370, 675); // Use this for testing on PC
  cells = new Life();
  cellsCopy = new Life();
}

void draw()
{
  background(0);
  if (accelerometerX>15 || accelerometerX<-15) clean();


  if (EDIT) { //EDITING MODE
    cells.Days = 0;
    cellsCopy.Days = 0;
    cells.pause = false;
    cells.draw();
  } else { //PLAYING MODE
    if ((cells.foundLife() || cellsCopy.foundLife())&&!cells.pause) { //if any cells are alive
      cells.Days++;
      cellsCopy.Days++;
    } 
    
    cells.checkFlipping(accelerometerZ);

    if (!cells.pause) { //Pausing a game when flipping (additional)
      //cells.Days++;
      //cellsCopy.Days++;
      cellsCopy.copy(cells); //cellsCopy = copy of cells
      cellsCopy.update(cells); ///update cellsCopy by referencing to cells
      cells.copy(cellsCopy); //swap: cells <-- cellsCopy
    }
    cells.draw(); //draw cells
  }

  fill(255);
  stroke(153);
  textSize(24);
  textAlign(CENTER, BOTTOM);
  text("Days: " + str(cells.Days), 50, height);
  delay(100);
}





void onTap(float x, float y)
{
  if (pinch || !EDIT) return;
  cells.clickCheck(x, y);
}

void onPinch(float x, float y, float d)
{
  if (pinch&&EDIT) {
    println("PINCH");
    pinchSize=pinchSize+d;
    if (pinchSize<0) pinchSize=0;
    if (pinchSize>1999) pinchSize=1999;
    int switchsize=int(pinchSize/400);

    cells.changeLifeSize(5-switchsize);
    cells.newLife();
    cellsCopy.changeLifeSize(5-switchsize);
    cellsCopy.newLife();
    delay(500);
  }
}

void mouseDragged()
{
  if (pinch || !EDIT) return;
  cells.clickCheck(mouseX, mouseY);
}

void mousePressed()
{
  if (pinch || !EDIT) return;
  cells.clickCheck(mouseX, mouseY);
}

void mouseReleased()
{
  cells.memoryStateChange();
}


public boolean surfaceTouchEvent(MotionEvent event)
{
  super.surfaceTouchEvent(event);
  int numPointers = event.getPointerCount();
  //if (numPointers>=2) delay(100);
  if (numPointers==5) {
    //gotFive = true;
    change();
    delay(100);
  }
  if (numPointers==3) {
   
    cells.cellColorChange();
    cellsCopy.cellColorChange();
      
  }
  if (numPointers==2) {
    delay(200);
    numPointers = event.getPointerCount();
    {
      if (numPointers==2) 
        pinch = true;

    }
  }

  else pinch=false;

  return gesture.surfaceTouchEvent(event);
}

void change()
{
  int temp=millis();
  if ((temp-changecheck)>500) EDIT=!EDIT;
  changecheck=temp;
}

void clean()
{
  if (!EDIT || pinch) return;
  println("CLEANED");
  cells.newLife();
}

void onAccelerometerEvent(float x, float y, float z)
{
  accelerometerX = x;
  accelerometerY = y;
  accelerometerZ = z;
}